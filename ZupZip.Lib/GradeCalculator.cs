﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZupZip.Lib.Interfaces;

namespace ZupZip.Lib
{

    public class GradeCalculator : IGradeCalculator
    {
        private List<GradeRate> rates;
        private int currentIndex;

        public GradeCalculator()
        {
            rates = new List<GradeRate>() 
            { 
                new GradeRate(85, "A"), 
                new GradeRate(70, "B"), 
                new GradeRate(60, "C"), 
                new GradeRate(50, "D"), 
                new GradeRate(0, "F"), 
            };
            rates = rates.OrderBy(r => r.MinScore).ToList();//0, 50, 60, 70, 85
            currentIndex = rates.Count - 1;//4
        }

        public string GetGradeForScore(double score)
        {
            if (score > 100 || score < 0) 
                throw new InvalidOperationException("Score must be between 0 to 100");            

            if (score >= rates[currentIndex].MinScore)
            {
                var grade = rates[currentIndex].Grade;
                //reset index 
                currentIndex = rates.Count - 1;
                return grade; 
            }

            --currentIndex;
            return GetGradeForScore(score);
        }
    }
}
