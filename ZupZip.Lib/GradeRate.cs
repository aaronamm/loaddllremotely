﻿namespace ZupZip.Lib
{
    public class GradeRate
    {
        public double MinScore { get; set; }
        public string Grade { get; set; }

        public GradeRate(double minScore, string rate)
        {
            this.MinScore = minScore;
            this.Grade = rate;
        }
    }
}