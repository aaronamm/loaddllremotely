﻿using System;
using System.IO;
using System.Reflection;
using ZupZip.Lib.Interfaces;

namespace LoadDllRemotely
{
    public class Program
    {
        private const string ASSEMBLY_PATH =
                        @"C:\Projects\LoadDllRemotely\ZupZip.Lib\bin\Debug\ZupZip.Lib.dll";
        private const string ASSEMBLY_URL =
                        @"http://codesanook.cloudapp.net/dll/ZupZip.Lib.dll";

        public static void Main(string[] args)
        {
            //LoadLocally();
            LoadRemotely();
        }

        public static void appDomain_DomainUnload(object sender, EventArgs e)
        {
            Console.WriteLine("domain name: {0} unloaded",
               ((AppDomain) sender).FriendlyName);
        }


        public static void LoadLocally()
        {
            var appDomain = AppDomain.CreateDomain("dynamicDll");
            appDomain.DomainUnload += new EventHandler(appDomain_DomainUnload);
            var proxy = (Proxy)appDomain.CreateInstanceAndUnwrap(
                typeof(Proxy).Assembly.FullName,
                typeof(Proxy).FullName);

            var calculator = proxy.GetObject<IGradeCalculator>(
                ASSEMBLY_PATH,
                "ZupZip.Lib.GradeCalculator");
            var score = 80;
            Console.WriteLine("you got grad: {0} from score: {1}", calculator.GetGradeForScore(score), score);

            AppDomain.Unload(appDomain);

            File.Delete(ASSEMBLY_PATH);//you can delete referece dll after remove 
        }


        public static void LoadRemotely()
        {
            //app domain setup 
            //load as byte array
            var appDomain = AppDomain.CreateDomain("dynamicDll");
            appDomain.DomainUnload += new EventHandler(appDomain_DomainUnload);

            var proxy = (Proxy)appDomain.CreateInstanceAndUnwrap(
                typeof(Proxy).Assembly.FullName,
                typeof(Proxy).FullName);

            var calculator = proxy.GetObject<IGradeCalculator>(
                ASSEMBLY_URL,
                "ZupZip.Lib.GradeCalculator");
            var score = 80;
            Console.WriteLine("you got grad: {0} from score: {1}", calculator.GetGradeForScore(score), score);

            AppDomain.Unload(appDomain);

        }

    }

    public class Proxy : MarshalByRefObject
    {
        public T GetObject<T>(
            string assemblyPath,
            string fullTypeName,
            params string[] referencedAssembliesPath) where T : class
        {
            try
            {
                //find reference for reference that will be load
                var assemblyToLoad = Assembly.ReflectionOnlyLoadFrom(assemblyPath);
                AssemblyName[] referencedAssemblies = assemblyToLoad.GetReferencedAssemblies();
                foreach (var referencedAssembly in referencedAssemblies)
                {
                    Console.WriteLine("referenceAssemblyName: {0}", referencedAssembly.Name);
                    Console.WriteLine("referenceAssemblyFullName: {0}", referencedAssembly.FullName);
                }

                //this dll will load in new domain
                var assembly = Assembly.LoadFrom(assemblyPath);
                var type = assembly.GetType(fullTypeName);
                var obj = (T)Activator.CreateInstance(type);
                return obj;
            }
            catch (Exception ex)
            {
                // throw new InvalidOperationException(ex);
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return null;
            }
        }

    }

}
